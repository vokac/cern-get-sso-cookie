%{!?dist: %define dist .slc5}
Name: @PACKAGE@
Version: @VERSION@
Release: @RELEASE@%{?dist}
Summary: A script to get CERN SSO Authentication cookie for command line tools. 
Group: CERN/Utilities
Source0: %{name}-%{version}.tar.gz
License: GPL
Vendor: CERN
Packager: @EMAIL@
URL: http://cern.ch/linux/
BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: noarch

Requires: perl >= 5.8.5
Requires: perl-Authen-Krb5 >= 1.5
Requires: perl-WWW-CERNSSO-Auth >= %{version}-%{release}

%if 0%{?rhel} >=6
Requires: CERN-CA-certs >= 20120322-8.slc6
Requires: ca-certificates >= 2010.63-3.el6_1.5
%else
Requires: CERN-CA-certs >= 20120322-8.slc5
Requires: openssl
%endif

%description
@PACKAGE@ is a command line utility which allows to acquire CERN Single Sign On 
cookies using Kerberos credentials or User certificates for later use with 
command line utilities alike curl, wget and other.

%package -n perl-WWW-CERNSSO-Auth
Summary: Perl extension interface for CERN SSO
Group: Development/Libraries

Requires: perl >= 5.8.5
Requires: perl-WWW-Curl >= 4.09

%if 0%{?rhel} == 6
Requires: libcurl >= 7.19.7-53.el6_9
%endif

%if 0%{?rhel} == 7
Requires: libcurl >= 7.29.0-25.el7
%endif

%description -n perl-WWW-CERNSSO-Auth
perl-WWW-CERNSSO-Auth (WWW::CERNSSO::Auth) provides perl extension interface for 
CERN Single Sign On system.

%prep

%setup

%build
make all

%install
mkdir -p $RPM_BUILD_ROOT/

%if "%{dist}" == ".slc5"
make install DESTDIR=$RPM_BUILD_ROOT/ PERLLIBDIR=/usr/lib/perl5/vendor_perl
%else
make install DESTDIR=$RPM_BUILD_ROOT/ PERLLIBDIR=/usr/share/perl5/vendor_perl
%endif
%clean
rm -rf $RPM_BUILD_ROOT


%files 
%defattr(-, root, root)
/usr/bin/cern-get-sso-cookie
/usr/share/man/man3/cern-get-sso-cookie.*

%files -n perl-WWW-CERNSSO-Auth
%if "%{dist}" == ".slc5"
/usr/lib/perl5/vendor_perl/WWW/CERNSSO/Auth.pm
%else
/usr/share/perl5/vendor_perl/WWW/CERNSSO/Auth.pm
%endif
/usr/share/man/man3/WWW::CERNSSO::Auth.*

%changelog
* Tue Jun 27 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.6-1
- fix for gitlab strange redirects (to different places 
  depending if GET or POST is used ..)

* Wed Apr 12 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.5.9-1
- fix for updated libcurl on SLC 6.9

* Wed Nov 30 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.5.8-1
- fix broken logic in certs key handling.

* Fri Oct 14 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.5.7-1
- include p12 certs patch from Giulio Eulisse.
* Thu Jan 14 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.5.6-1
- and fix it again for updated libcurl on CentOS 7.2 ..

* Fri Aug 07 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.5.5-1
- fix for curl 'negotiate' connection reuse vulnerability patch
  side effects (breaking the tool on SLC6.7)

* Wed Jul 22 2015   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.5.4-1
- longer libcurl timeout.

* Mon Jun 17 2013   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.4.4-1
- changed user agent to include 'Mozilla' - to make Sharepoint think
  we are a browser and redirect to SSO.

* Tue Nov 27 2012   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.4.2-2
- correct dependencies.

* Fri Nov 16 2012   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.4.2-1
- fixed --nocertverify option on SLC5 

* Tue Aug 14 2012   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.4-1
- added cookie file reprocessing

* Tue Jul 10 2012   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.2-6
- spec file fixes.

* Mon Jul  9 2012   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.2-1
- added missing constants for SLC5

* Fri Jul  6 2012   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.1-1
- initial release.
